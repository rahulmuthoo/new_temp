#include "DECAF_main.h"
#include "DECAF_types.h"
#include "DECAF_target.h"
#include "hookapi.h"
#include <Output.h>
#include "DECAF_callback.h"
#include "DECAF_callback_common.h"
#include "vmi_callback.h"
#include <stdio.h>
#include "function_map.h"
#include "shared/tainting/taint_memory.h"
#include "shared/tainting/taintcheck_opt.h"
#include <sys/time.h>
#include "trace.h"

static plugin_interface_t callback_interface;
static DECAF_Handle temp_handle1 = DECAF_NULL_HANDLE;
static uint32_t targetcr3 = 0;
unsigned int taint_memory = 0;
unsigned int taint_mem_size = 0;

uint32_t call_stack[12];

int get_rand()
{  
   srand(time(NULL));
   int x = rand() & 0xffff;
   x |= (rand() & 0xffff) << 16;
   return x;
}

void taint_virt_mem(uint32_t addr, uint32_t size)
{
	uint32_t i,phy_addr;
	uint8_t taint;
	for (i = 0;i<size;i++){
		phy_addr =  DECAF_get_phys_addr(cpu_single_env, addr+i);
		taint = get_rand();
		taint_mem(phy_addr,1,&taint);
	}

}

void cleanup(){
}

static void read_taint(DECAF_Callback_Params* params)
{
				printf("*************In read_taint*************************\n");
				printf("Vaddr:: %08x, paddr:: %08x size:: %d \n", params->rt.vaddr, params->rt.paddr, params->rt.size);
}

static void register_taint(uint32_t addr, uint32_t size){
				taint_virt_mem(call_stack_read[2], call_stack_read[3]);
				temp_handle1 = DECAF_register_callback(DECAF_READ_TAINTMEM_CB, read_taint, NULL);
}

plugin_interface_t * init_plugin() {
				callback_interface.plugin_cleanup = &cleanup;
				callback_interface.mon_cmds = NULL;
				callback_interface.info_cmds = NULL;

				register_taint(0x12312312, 10); // 0x12312312 -- dummy virtual address to taint
																				// 10 -- size of memory to taint
																				// these are actually derived from stack of the target vm
				return &callback_interface;
}
